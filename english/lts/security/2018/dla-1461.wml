<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>ClamAV, an anti-virus utility for Unix, has released the version 0.100.1.
Installing this new version is required to make use of all current virus
signatures and to avoid warnings.</p>

<p>This version also fixes two security issues discovered after version 0.100.0:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-0360">CVE-2018-0360</a>

    <p>Integer overflow with a resultant infinite loop via a crafted Hangul Word
    Processor file. Reported by Secunia Research at Flexera.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-0361">CVE-2018-0361</a>

    <p>PDF object length check, unreasonably long time to parse a relatively small
    file. Reported by aCaB.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
0.100.1+dfsg-0+deb8u1.</p>

<p>We recommend that you upgrade your clamav packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1461.data"
# $Id: $
