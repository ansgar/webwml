#use wml::debian::translation-check translation="244380ed6414a14f265795cff6ac8dab1d04e3a3" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs problèmes de sécurité ont été corrigés dans plusieurs
démultiplexeurs et décodeurs dans la bibliothèque multimedia libav.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-8542">CVE-2014-8542</a>

<p>libavcodec/utils.c omettait un certain ID de codec lors de l’application
de l’alignement. Cela permettait à des attaquants distants de provoquer un déni de
service (accès hors limites) ou éventuellement avait un impact non précisé à
l’aide de données JV contrefaites.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-1207">CVE-2015-1207</a>

<p>Une vulnérabilité de double libération dans libavformat/mov.c permettait à
des attaquants distants de provoquer un déni de service (corruption de mémoire et
plantage) à l'aide d'un fichier .m4a contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7863">CVE-2017-7863</a>

<p>libav avait une écriture hors limites causée par un dépassement de tampon
basé sur le tas, relatif à la fonction decode_frame_common dans
libavcodec/pngdec.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7865">CVE-2017-7865</a>

<p>libav avait une écriture hors limites causée par un dépassement tampon basé
sur le tas relatif à la fonction ipvideo_decode_block_opcode_0xA dans
libavcodec/interplayvideo.c et la fonction avcodec_align_dimensions2
dans libavcodec/utils.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14169">CVE-2017-14169</a>

<p>Dans la fonction mxf_read_primer_pack dans libavformat/mxfdec.c, une erreur
de signature d’entier pouvait se produire lorsqu’un fichier contrefait, réclamant
un champ large <q>item_num</q> tel que 0xffffffff, était fourni. Comme résultat,
la variable <q>item_num</q> devenait négative, contournant la vérification pour
pour une grande valeur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14223">CVE-2017-14223</a>

<p>Dans libavformat/asfdec_f.c un déni de service dans asf_build_simple_index()
dû à un manque de vérification d’EOF (End of File) pouvait provoquer une énorme
consommation de CPU. Quand un fichier ASF contrefait, réclamant un champ
<q>ict</q> large dans l’entête mais ne contenant pas de données de sauvegarde
suffisantes, était fourni, la boucle for pouvait consommer de larges ressources
de CPU et mémoire, puisqu’il n’y avait pas de vérification EOF dans la boucle.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans la version 6:11.12-1~deb8u5.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libav.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1654.data"
# $Id: $
