<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Brandon Perry discovered that xerces-c, a validating XML parser library
for C++, fails to successfully parse a DTD that is deeply nested,
causing a stack overflow. A remote unauthenticated attacker can take
advantage of this flaw to cause a denial of service against applications
using the xerces-c library.</p>

<p>Additionally this update includes an enhancement to enable applications
to fully disable DTD processing through the use of an environment
variable (XERCES_DISABLE_DTD).</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
3.1.1-3+deb7u4.</p>

<p>We recommend that you upgrade your xerces-c packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-535.data"
# $Id: $
