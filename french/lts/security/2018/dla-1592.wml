#use wml::debian::translation-check translation="ab3be4ee01879fd4484c795bbaa824377c218575" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Deux vulnérabilités de sécurité ont été découvertes dans OTRS, un système de
requête d’assistance (Ticket Request System) qui pourraient conduire à une
augmentation de droits ou à l’écriture de fichier arbitraire.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19141">CVE-2018-19141</a>

<p>Un attaquant connecté dans OTRS comme utilisateur administrateur peut
manipuler l’URL pour provoquer l’exécution de JavaScript dans le contexte
d’OTRS.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19143">CVE-2018-19143</a>

<p>Un attaquant connecté dans OTRS comme utilisateur peut manipuler le
formulaire de soumission pour provoquer la suppression de fichiers arbitraires
dont l’utilisateur du serveur web OTRS possède les droits d’écriture.</p></li>

</ul>

<p>Veuillez aussi lire l’alerte de l’amont pour
<a href="https://security-tracker.debian.org/tracker/CVE-2018-19141">CVE-2018-19141</a>.
Si vous pensez être affecté, vous devriez songer à exécuter les déclarations SQL
de nettoyage mentionnées pour enlever les enregistrements pouvant être touchés.</p>

<p><url "https://community.otrs.com/security-advisory-2018-09-security-update-for-otrs-framework/"></p>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans la version 3.3.18-1+deb8u7.</p>

<p>Nous vous recommandons de mettre à jour vos paquets otrs2.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1592.data"
# $Id: $
