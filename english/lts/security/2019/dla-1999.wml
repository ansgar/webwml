<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Multiple vulnerabilities have been found in the Symfony PHP framework
which could lead to a timing attack/information leak, argument injection
and code execution via unserialization.</p>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
2.3.21+dfsg-4+deb8u6.</p>

<p>We recommend that you upgrade your symfony packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1999.data"
# $Id: $
